#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# install phpunit
wget https://phar.phpunit.de/phpunit-4.phar
wget https://phar.phpunit.de/phpunit-5.phar
wget https://phar.phpunit.de/phpunit-6.phar
wget https://phar.phpunit.de/phpunit-7.phar
chmod +x phpunit-4.phar phpunit-5.phar phpunit-6.phar phpunit-7.phar
# php 5.3, 5.4, 5.5
mv phpunit-4.phar /usr/local/bin/phpunit4
# php 5.6
mv phpunit-5.phar /usr/local/bin/phpunit5
# php 7.0
mv phpunit-6.phar /usr/local/bin/phpunit6
# php 7.1, 7.2
mv phpunit-7.phar /usr/local/bin/phpunit7