<?php

include_once '../vendor/autoload.php';

use Parser\Expression;

$act = isset($_POST['act']) ? $_POST['act'] : '';
$result = null;

if ($act === 'eval') {
    $expr = isset($_POST['expression']) ? $_POST['expression'] : '';

    try {
        $ex = new Expression($expr);
        $result = $ex->evaluate();
    } catch (Exception $e) {
        print $e->getMessage();
    }
}

print "
    <form method='post' action='calc.php'>
    <textarea name='expression' required='required'>{$expr}</textarea>
    <input type='hidden' name='act' value='eval'>
    <br>
    <input type='submit' value='Evaluate'>
    <input type='button' value='Clear' id='expressionClearBtn'>
</form>
";

print "<script>
    window.onload = function (ev) { 
        document.getElementById('expressionClearBtn').onclick = function (ev2) { 
            this.form.elements.namedItem('expression').value = '';
            document.getElementById('result').innerText = '';
         }
     };
</script>";

if (!is_null($result)) {
    print "<br>";
    print "<div id='result'>" . $result . "</div>";
}