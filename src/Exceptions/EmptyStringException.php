<?php

namespace Exceptions;

class EmptyStringException extends \Exception
{
    public function __construct()
    {
        $this->message = 'String is empty!';
    }
}