<?php

namespace Exceptions;

class WrongExpressionFormatException extends \Exception
{
    public function __construct()
    {
        $this->message = 'Wrong expression format!';
    }
}