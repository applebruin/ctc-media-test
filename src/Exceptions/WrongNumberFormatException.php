<?php
namespace Exceptions;

class WrongNumberFormatException extends \Exception
{
    public function __construct($value)
    {
        $this->message = 'Wrong number format: ' . $value;
    }
}