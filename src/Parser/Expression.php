<?php

namespace Parser;

use Exceptions\EmptyStringException;
use Exceptions\WrongExpressionFormatException;
use Parser\Token\OperatorToken;
use Parser\Token\NumberToken;

class Expression
{
    /**
     * @var string
     */
    private $originalString;

    /**
     * @var Token[]
     */
    private $parsedTokens;

    /**
     * @var Token[]
     */
    private $postfixTokens;

    /**
     * @var mixed
     */
    private $evaluatedResult;

    /**
     * @var bool
     */
    private $isEvaluated = false;

    /**
     * @param string $string
     */
    public function __construct($string)
    {
        $this->originalString = $string;
    }

    /**
     * @return Token[]
     * @throws \Exception
     */
    public function parseOriginalString()
    {
        $string = str_replace(' ', '', $this->originalString);

        if (empty($string)) {
            throw new EmptyStringException();
        }

        /** @var Token[] $parsedTokens */
        $parsedTokens = [];
        //переменная для хранения строки-числа
        $currentNumber = '';
        //переменная для подсчёта скобок
        $bracketsCounter = [];

        for ($s = 0; $s < strlen($string); $s++) {
            $symb = $string[$s];

            if (!$this->isPartOfNumber($symb) && !$this->isOperator($symb)) {
                throw new WrongExpressionFormatException();
            }

            //проверяем, что символ входит в число и оно может быть отрицательным
            $isPartOfNumber =
                $this->isPartOfNumber($symb)
                ||
                ($s === 0 || $string[$s - 1] === '(') && $symb === '-';

            if ($isPartOfNumber) {
                $currentNumber .= $symb;
            }

            //сохраняем число
            if (
                (!$isPartOfNumber || $s == strlen($string) - 1)
                &&
                !empty($currentNumber)
            ) {
                array_push($parsedTokens, new NumberToken($currentNumber));
                $currentNumber = '';
            }
            //созраняем оператор, исключая '-' перед отрицательными числами
            if (!$isPartOfNumber && $this->isOperator($symb)) {
                //проверим, что у нас корректное число скобок
                switch ($symb) {
                    case '(':
                        array_push($bracketsCounter, $symb);
                        break;

                    case ')':
                        //если закрывающих больше открывающих
                        if (empty($bracketsCounter)) {
                            throw new WrongExpressionFormatException();
                        }

                        array_pop($bracketsCounter);
                        break;

                    default:
                        //если строка заканчивается на операторе, то это некорректная строка
                        if ($s === strlen($string) - 1) {
                            throw new WrongExpressionFormatException();
                        }
                }

                array_push($parsedTokens, new OperatorToken($symb));
            }
        }

        //если открывающих больше закрывающих
        if (!empty($bracketsCounter)) {
            throw new WrongExpressionFormatException();
        }

        $this->parsedTokens = $parsedTokens;

        return $parsedTokens;
    }

    /**
     * @return Token[]
     * @throws \Exception
     */
    public function getPostfixNotice()
    {
        if ($this->parsedTokens === null) {
            $this->parseOriginalString();
        }

        if (is_null($this->postfixTokens)) {
            /** @var OperatorToken[] $operatorsStack */
            $operatorsStack = [];
            /** @var Token[] $postfixTokens */
            $postfixTokens = [];

            foreach ($this->parsedTokens as $token) {
                if ($token->isNumber()) {
                    array_push($postfixTokens, $token);
                }

                if ($token->isOperator()) {
                    switch ($token->getValue()) {
                        case '(':
                            array_push($operatorsStack, $token);
                            break;

                        case ')':
                            while ($lastOperator = array_pop($operatorsStack)) {

                                if ($lastOperator->getValue() === '(') {
                                    break;
                                } else {
                                    array_push($postfixTokens, $lastOperator);
                                    if (empty($operatorsStack)) {
                                        throw new WrongExpressionFormatException();
                                    }
                                }
                            }
                            break;

                        default:
                            if (!empty($operatorsStack)) {
                                $lastOperator = end($operatorsStack);

                                while (
                                    $lastOperator !== false
                                    &&
                                    $lastOperator->getPriority() >= $token->getPriority()
                                ) {
                                    array_push($postfixTokens, array_pop($operatorsStack));
                                    $lastOperator = end($operatorsStack);
                                }
                            }
                            array_push($operatorsStack, $token);
                            break;
                    }
                }
            }

            while (!empty($operatorsStack)) {
                $op = array_pop($operatorsStack);
                if ($op->getValue() === '(') {
                    throw new WrongExpressionFormatException();
                }
                array_push($postfixTokens, $op);
            }

            $this->postfixTokens = $postfixTokens;
        }

        return $this->postfixTokens;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function evaluate()
    {
        if (!$this->isEvaluated) {
            $this->parseOriginalString();
            $this->getPostfixNotice();

            $resultTokens = [];
            foreach ($this->postfixTokens as $token) {
                if ($token->isOperator()) {
                    $var1 = array_pop($resultTokens);
                    $var2 = array_pop($resultTokens);

                    if (is_null($var1) || is_null($var2)) {
                        throw new WrongExpressionFormatException();
                    }

                    switch ($token->getValue()) {
                        case '+':
                            array_push($resultTokens, $var2 + $var1);
                            break;
                        case '-':
                            array_push($resultTokens, $var2 - $var1);
                            break;
                        case '*':
                            array_push($resultTokens, $var2 * $var1);
                            break;
                        case '/':
                        case ':':
                            array_push($resultTokens, $var2 / $var1);
                            break;
                        case '%':
                            array_push($resultTokens, $var2 % $var1);
                            break;
                        case '^':
                            array_push($resultTokens,  pow($var2, $var1));
                            break;
                    }
                } else {
                    array_push($resultTokens, $token->getValue());
                }
            }

            if (count($resultTokens) === 1) {
                $this->evaluatedResult = $resultTokens[0];
            } else {
                throw new WrongExpressionFormatException();
            }
        }

        return $this->evaluatedResult;
    }

    /**
     * @param string $symb
     * @return bool
     */
    private function isPartOfNumber($symb)
    {
        return is_numeric($symb) || $symb === '.';
    }

    /**
     * @param string $symb
     * @return bool
     */
    private function isOperator($symb)
    {
        return in_array($symb, OperatorToken::$operators);
    }
}