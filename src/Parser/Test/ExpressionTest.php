<?php

namespace Parser;

use Exceptions\EmptyStringException;
use Exceptions\WrongExpressionFormatException;
use Parser\Token\NumberToken;
use Parser\Token\OperatorToken;

class ExpressionTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @param Token[] $expected
     * @param string $string
     * @param \Exception | null $expectedException
     * @throws \Exception
     * @dataProvider dataParseString
     */
    public function testParseOriginalString(array $expected, $string, $expectedException = null)
    {
        $expr = new Expression($string);

        if (!is_null($expectedException)) {
            $this->expectException($expectedException);
        }

        $this->assertEquals($expected, $expr->parseOriginalString());
    }

    /**
     * @return array
     * @throws \Exceptions\WrongNumberFormatException
     */
    public function dataParseString()
    {
        return [
            'normal string' => [
                [
                    (new NumberToken('5')),
                    (new OperatorToken('+')),
                    (new OperatorToken('(')),
                    (new NumberToken('-7')),
                    (new OperatorToken(')'))
                ],
                '5 + (-7)',
                null
            ],

            'empty string' => [
                [],
                '',
                EmptyStringException::class
            ],

            'incorrect symbol' => [
                [],
                '5 + 1 - n',
                WrongExpressionFormatException::class
            ],

            'incorrect operators' => [
                [],
                '5 + 1 -',
                WrongExpressionFormatException::class
            ],

            'incorrect brackets )' => [
                [],
                '5 + 1)',
                WrongExpressionFormatException::class
            ],

            'incorrect brackets (' => [
                [],
                '5 + (1',
                WrongExpressionFormatException::class
            ]
        ];
    }

    /**
     * @param array $expected
     * @param string $string
     * @param \Exception | null $exception
     * @dataProvider dataGetPostfixNotice
     * @throws \Exception
     */
    public function testGetPostfixNotice(array $expected, $string, $exception = null)
    {
        if (!is_null($exception)) {
            $this->expectException($exception);
        }

        $expr = new Expression($string);

        $this->assertEquals($expected, $expr->getPostfixNotice());
    }

    /**
     * @return array
     * @throws \Exceptions\WrongNumberFormatException
     */
    public function dataGetPostfixNotice()
    {
        return [
            'normal' => [
                [
                    (new NumberToken(20)),
                    (new NumberToken(3)),
                    (new NumberToken(5)),
                    (new OperatorToken('*')),
                    (new OperatorToken('-'))
                ],
                '20 - 3 * 5',
                null
            ],

            'with brackets' => [
                [
                    (new NumberToken(4)),
                    (new NumberToken(3)),
                    (new OperatorToken('-')),
                    (new NumberToken(5)),
                    (new OperatorToken('*')),
                ],
                '(4 - 3) * 5',
                null
            ],

            'incorrect opening brackets' => [
                [],
                '(4 + 5',
                WrongExpressionFormatException::class
            ],

            'incorrect closing brackets' => [
                [],
                '4 + 5)',
                WrongExpressionFormatException::class
            ]
        ];
    }

    /**
     * @param mixed $expected
     * @param string $string
     * @param \Exception | null $exception
     * @throws \Exception
     * @dataProvider dataEvaluate
     */
    public function testEvaluate($expected, $string, $exception = null)
    {
        if (!is_null($exception)) {
            $this->expectException($exception);
        }

        $expr = new Expression($string);
        $this->assertEquals($expected, $expr->evaluate());
    }

    /**
     * @return array
     */
    public function dataEvaluate()
    {
        return [
            'normal' => [
                5.5,
                '0.5 + 2.5*2',
                null
            ],

            'with brackets' => [
                -1,
                '7 - (2*2 + 2^2)',
                null
            ],

            'incorrect operators' => [
                null,
                '7 + - 5',
                WrongExpressionFormatException::class
            ]
        ];
    }
}
