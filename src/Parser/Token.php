<?php

namespace Parser;

/**
 * Class Token
 * @package Parser
 */
abstract class Token {
    const TYPE_NUM = 'num';
    const TYPE_OPERATOR = 'operator';

    protected $type;

    /** @var string|double|int */
    protected $value;

    /**
     * Token constructor.
     * @param string $value
     * @param string $type
     */
    public function __construct($value, $type)
    {
        $this->type = $type;
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * проверяет, что токен является оператором
     * @return bool
     */
    public function isOperator()
    {
        return $this->type === self::TYPE_OPERATOR;
    }

    /**
     * проверяет, что токен является числом
     * @return bool
     */
    public function isNumber()
    {
        return $this->type === self::TYPE_NUM;
    }
}