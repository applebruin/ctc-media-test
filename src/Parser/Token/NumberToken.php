<?php

namespace Parser\Token;

use Exceptions\WrongNumberFormatException;
use Parser\Token;

class NumberToken extends Token
{
    /**
     * @param string $value
     * @throws WrongNumberFormatException
     */
    public function __construct($value)
    {
        if (preg_match('/^\-?\d+\.\d+$/', $value)) {
            $parsedValue = floatval($value);
        } else if (preg_match('/^\-?\d+$/', $value)) {
            $parsedValue = intval($value);
        } else {
            throw new WrongNumberFormatException($value);
        }

        parent::__construct($parsedValue, Token::TYPE_NUM);
    }
}