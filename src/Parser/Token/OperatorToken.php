<?php

namespace Parser\Token;

use Parser\Token;

class OperatorToken extends Token
{
    public static $operators = ['+', '-', '*', '/', '%', '^', ':', '(', ')'];

    private static $opPriorities = [
        '+' => 10,
        '-' => 10,
        '*' => 20,
        '/' => 20,
        ':' => 20,
        '%' => 20,
        '^' => 30,
        '(' => 0,
        ')' => 0
    ];

    /**
     * @param string $value
     */
    public function __construct($value)
    {
        parent::__construct($value, Token::TYPE_OPERATOR);
    }

    public function getPriority()
    {
        return self::$opPriorities[$this->value];
    }
}
